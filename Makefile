CROSS_COMPILE ?=
CC ?= $(CROSS_COMPILE)gcc
CFLAGS += -Wall -g

EXE =                    \
  ioctl-gpio-list        \
  ioctl-invert-gpio      \
  ioctl-poll-gpio        \
  ioctl-toggle-gpio      \
  ioctl-fast-toggle-gpio \


.PHONY: all

all:$(EXE)


.PHONY: clean

clean:
	rm -f $(EXE) *.o


